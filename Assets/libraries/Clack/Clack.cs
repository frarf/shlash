﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XInputDotNetPure;

public class ClackKeyboard {
	Dictionary<string, Action> actions = new Dictionary<string, Action>();
	Dictionary<string, string> binds = new Dictionary<string, string>();

	public void Bind(string input, Action func) {
		actions[input] = func;
	}

	public void Bind(string input, string name) {
		binds[name] = input;
	}

	public void Unbind(string name) {
		binds[name] = null;
	}

	public void Update() {
		foreach(KeyValuePair<string, Action> action in actions) {
			if (Inputs.KeyboardDown(action.Key))
				action.Value();
		}
	}

	public bool Down(string name) {
		try { return Inputs.KeyboardDown(binds[name]);}
		catch (KeyNotFoundException e) { throw new KeyNotFoundException(String.Format("Keyboard key '{0}' does not exist. Bind this key first.", name)); }
		catch (ArgumentException e) { throw new KeyNotFoundException(String.Format("Keyboard key '{0}' does not exist. Bind this key first.", name)); }
	}

	public bool Pressed(string name) {
		try { return Inputs.KeyboardPressed(binds[name]);}
		catch (KeyNotFoundException e) { throw new KeyNotFoundException(String.Format("Keyboard key '{0}' does not exist. Bind this key first.", name)); }
		catch (ArgumentException e) { throw new KeyNotFoundException(String.Format("Keyboard key '{0}' does not exist. Bind this key first.", name)); }
	}

	public bool Released(string name) {
		try { return Inputs.KeyboardReleased(binds[name]);}
		catch (KeyNotFoundException e) { throw new KeyNotFoundException(String.Format("Keyboard key '{0}' does not exist. Bind this key first.", name)); }
		catch (ArgumentException e) { throw new KeyNotFoundException(String.Format("Keyboard key '{0}' does not exist. Bind this key first.", name)); }
	}

	public float Get(string name) {
		return Inputs.KeyboardPressed(binds[name]) ? 1 : 0;
	}
}

public class ClackGamepad {
	private GamePadState currState;
    private GamePadState prevState;
	private PlayerIndex controllerIndex;

	private Dictionary<string, Action> actions = new Dictionary<string, Action>();
	private Dictionary<string, string> binds = new Dictionary<string, string>();

	public ClackGamepad(bool getFirstGamepad = false) {
		int startIndex = (getFirstGamepad) ? 0 : Inputs.nextControllerIndex;

		for (int i = startIndex; i < 4; i++) {
				PlayerIndex testControllerIndex = (PlayerIndex)i;
				GamePadState testState = GamePad.GetState(testControllerIndex);
				if (testState.IsConnected) {
					Debug.Log(string.Format("Found {0}", testControllerIndex));
					controllerIndex = testControllerIndex;
				}
			}

		if (!getFirstGamepad) Inputs.nextControllerIndex++;
	}

	public void Bind(string input, Action func) {
		actions[input] = func;
	}

	public void Bind(string input, string name) {
		binds[name] = input;
	}

	public void Unbind(string name) {
		binds[name] = null;
	}

	public void Update() {
		prevState = currState;
		currState = GamePad.GetState(controllerIndex);

		foreach(KeyValuePair<string, Action> action in actions) {
			Inputs.AssertIsControllerInput(action.Key);

			if (Inputs.ControllerDown(action.Key, currState, prevState) || Inputs.DPadDown(action.Key, currState, prevState))
				action.Value();
		}
	}

	public bool Down(string name) {
		AssertIsBind(name);
		Inputs.AssertIsControllerInput(binds[name]);

		return Inputs.ControllerDown(binds[name], currState, prevState) || Inputs.DPadDown(binds[name], currState, prevState);
	}

	public bool Pressed(string name) {
		AssertIsBind(name);
		Inputs.AssertIsControllerInput(binds[name]);

		return Inputs.ControllerPressed(binds[name], currState) || Inputs.DPadPressed(binds[name], currState);
	}

	public bool Released(string name) {
		AssertIsBind(name);
		Inputs.AssertIsControllerInput(binds[name]);

		return Inputs.ControllerReleased(binds[name], currState, prevState) || Inputs.DPadReleased(binds[name], currState, prevState);
	}

	public float Get(string name) {
		AssertIsBind(name);

		bool isAnalog = Inputs.axisToAnalog.ContainsKey(binds[name]);
		bool isTrigger = Inputs.triggerToGamepad.ContainsKey(binds[name]);
		
		if (isAnalog) {
			Analog analog = Inputs.axisToAnalog[binds[name]];
			return analog.isX ? currState.ThumbSticks.Get(analog.thumbstickName).X : currState.ThumbSticks.Get(analog.thumbstickName).X;
		}
		else if (isTrigger) {
			return currState.Triggers.Get(Inputs.triggerToGamepad[binds[name]]);
		}
		else {
			return Pressed(name) ? 1 : 0;
		}
	}

	private void AssertIsBind(string name) {
		if (!binds.ContainsKey(name))
			throw new KeyNotFoundException(String.Format("Gamepad bind '{0}' does not exist. Bind it first.", name));	
	}
}

static class Inputs {
	public static int nextControllerIndex = 0;

	public static Dictionary<string, string> buttonToGamepad = new Dictionary<string, string>() {
		{"fdown", "A"},
		{"fright", "B"},
		{"fleft", "X"},
		{"fup", "Y"},
		{"leftshoulder", "LeftShoulder"},
		{"rightshoulder", "RightShoulder"},
		{"leftstick", "LeftStick"},
		{"rightstick", "RightStick"},
		{"start", "Start"},
		{"select", "Back"}
	};

	public static Dictionary<string, string> dpadToGamepad = new Dictionary<string, string>() {
		{"dpdown", "Down"},
		{"dpup", "Up"},
		{"dpright", "Right"},
		{"dpleft", "Left"},
	};

	public static Dictionary<string, Analog> axisToAnalog = new Dictionary<string, Analog>() {
		{"leftstickx", new Analog("Left", true)},	
		{"rightstickx", new Analog("Right", true)},
		{"leftsticky", new Analog("Left", false)},	
		{"rightsticky", new Analog("Right", false)}
	};


	public static Dictionary<string, string> triggerToGamepad = new Dictionary<string, string>() {
		{"lefttrigger", "Left"},
		{"righttrigger", "Right"}
	};
	
	public static bool KeyboardDown(string key) {
		return Input.GetKeyDown(key);
	}

	public static bool KeyboardPressed(string key) {
		return Input.GetKey(key);
	}

	public static bool KeyboardReleased(string key) {
		return Input.GetKeyUp(key);
	}

	public static bool ControllerDown(string key, GamePadState curr, GamePadState prev) {
		try { return prev.Buttons.Get(buttonToGamepad[key]) == ButtonState.Released && curr.Buttons.Get(buttonToGamepad[key]) == ButtonState.Pressed; }
		catch (KeyNotFoundException) {return false;}
	}

	public static bool ControllerPressed(string key, GamePadState curr) {
		try { return curr.Buttons.Get(buttonToGamepad[key]) == ButtonState.Pressed; }
		catch (KeyNotFoundException) {return false;}
	}

	public static bool ControllerReleased(string key, GamePadState curr, GamePadState prev) {
		try { return prev.Buttons.Get(buttonToGamepad[key]) == ButtonState.Pressed && curr.Buttons.Get(buttonToGamepad[key]) == ButtonState.Released; }
		catch (KeyNotFoundException) {return false;}
	}

	public static bool DPadDown(string key, GamePadState curr, GamePadState prev) {	
		try { return prev.DPad.Get(dpadToGamepad[key]) == ButtonState.Released && curr.DPad.Get(dpadToGamepad[key]) == ButtonState.Pressed; }
		catch (KeyNotFoundException) {return false;}
	}

	public static bool DPadPressed(string key, GamePadState curr) {
		try { return curr.DPad.Get(dpadToGamepad[key]) == ButtonState.Pressed; }
		catch (KeyNotFoundException) {return false;}
	}

	public static bool DPadReleased(string key, GamePadState curr, GamePadState prev) {
		try { return prev.DPad.Get(dpadToGamepad[key]) == ButtonState.Pressed && curr.DPad.Get(dpadToGamepad[key]) == ButtonState.Released; }
		catch (KeyNotFoundException) {return false;}
	}

	public static void AssertIsControllerInput(string input) {
		// If the button input is not a valid input			
		if (!Inputs.buttonToGamepad.ContainsKey(input) && !Inputs.dpadToGamepad.ContainsKey(input))
			throw new KeyNotFoundException(String.Format("Gamepad input '{0}' does not exist. Bind a valid input.", input));
	}
}

static class ConrollerExtensions {
    public static ButtonState Get(this GamePadButtons state, string propertyName) {
		return (ButtonState)state.GetType().GetProperty(propertyName).GetValue(state, null);
	}

    public static ButtonState Get(this GamePadDPad state, string propertyName) {
		return (ButtonState)state.GetType().GetProperty(propertyName).GetValue(state, null);
	}

    public static GamePadThumbSticks.StickValue Get(this GamePadThumbSticks state, string propertyName) {
		return (GamePadThumbSticks.StickValue)state.GetType().GetProperty(propertyName).GetValue(state, null);
	}

	public static float Get(this GamePadTriggers state, string propertyName) {
		return (float)state.GetType().GetProperty(propertyName).GetValue(state, null);
	}
}

// A way for me to hold two values in a dictionary,
// specifically for analogs
struct Analog {
	public string thumbstickName;
	public bool isX;

	public Analog(string thumbstickName, bool isX) {
		this.thumbstickName = thumbstickName;
		this.isX = isX;
	}
}