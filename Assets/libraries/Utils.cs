﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Utils {
    public static bool Between(float n, float min, float max) {
        return n > min && n < max;
    }

    public static float Angle(Vector2 from, Vector2 to) {
        float slopeAngle = Vector2.Angle(from, to);

        Vector3 cross = Vector3.Cross(from, to);

        if (cross.z > 0)
            slopeAngle = 360 - slopeAngle;

        return slopeAngle;
    }
}