﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnyAround : BehaviorAction {
	private LayerMask objectMask;
	private float radius;
	private Vector2 size;

	public AnyAround(LayerMask objectMask, float radius) : base("AnyAround") {
		this.objectMask = objectMask;
		this.radius = radius;
		this.size = new Vector2(radius, radius);
	}

	public override Status Run(ref Context context) {
		Collider2D collider = Physics2D.OverlapCapsule(gameObject.transform.position, size, CapsuleDirection2D.Horizontal, 0, objectMask);

		if (collider == null) {
			return Status.Failure;
		}

		context.anyAroundObject = collider.gameObject;
		Debug.Log(collider.gameObject);
		return Status.Success;
	}
}
