﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FindWanderPoint : BehaviorAction {
	private float rangeMin;
	private float rangeMax;

	public FindWanderPoint(float rangeMin, float rangeMax) : base("FindWanderPoint") {
		this.rangeMin = rangeMin;
		this.rangeMax = rangeMax;
	}

	public override Status Run(ref Context context) {
		float distance = Random.Range(rangeMin, rangeMax);
		
		Vector2 wanderPoint = new Vector2(context.gameObject.transform.position.x + distance, context.gameObject.transform.position.y);

		context.wanderPoint = wanderPoint;

		return Status.Success;
	}
}
