﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveToPoint : BehaviorAction {
	private Timer timer;
	private bool stuck;
	private CollisionInfo collisions;

	public MoveToPoint(): base("MoveToPoint") {

	}

	public override Status Run(ref Context context) {
		if (collisions.left || collisions.right)
			return Status.Failure;
			
		if (context.entity.behaviorOn["arrive"])
			return Status.Running;
		else
			return Status.Success;
	}

	public override void Start(Context context) {
		context.entity.steerable.ArriveOn(context.wanderPoint);
		collisions = context.entity.controller.collisions;
	}

	public override void Finish(Status status, Context context) {

	}
}
