﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wait : BehaviorAction {
	private float waitDuration;
	private bool done;

	public Wait(float waitDuration) : base("Wait") {
		this.waitDuration = waitDuration;
		this.done = false;
	}

	public override Status Run(ref Context context) {
		if (done) {
			return Status.Success;
		}
		return Status.Running;
	}

	public override void Start(Context context) {
		done = false;
		Timer.Register(waitDuration, () => done = true);
	}
}
