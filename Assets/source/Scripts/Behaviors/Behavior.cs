﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Behavior {
    protected Status status;
    public GameObject gameObject;

    public enum Status {Invalid = 1, Running = 2, Success = 3, Failure = 4};

    public Behavior() {
        this.status = Status.Invalid;
    }

    public virtual Status Update(ref Context context) {
        this.gameObject = context.gameObject;
        if (this.status != Status.Running) Start(context); 
        this.status = Run(ref context);
        if (this.status != Status.Running) Finish(status, context);
        return this.status;
    }

    public virtual void Start(Context context) {
        this.gameObject = context.gameObject;                
    }

    public virtual void Finish(Status status, Context context) {}

    public virtual Status Run(ref Context context) {
        this.gameObject = context.gameObject;                
        return Status.Invalid;
    }
}
