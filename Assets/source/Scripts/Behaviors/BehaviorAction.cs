﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BehaviorAction : Behavior {
	protected string name;

	public BehaviorAction(string name = "Action") : base() {
		this.name = name;
	}
}
