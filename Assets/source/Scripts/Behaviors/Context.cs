﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct Context {
	public GameObject gameObject;
	public Entity entity;
	public Vector2 wanderPoint;
	public GameObject anyAroundObject;

	public void Reset() {
		anyAroundObject = null;
	}
}
