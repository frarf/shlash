﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Decorator : Behavior {
	protected string name;
	protected Behavior behavior;

	public Decorator(string name, Behavior behavior) : base() {
		this.name = name;
		this.behavior = behavior;
	}
}
