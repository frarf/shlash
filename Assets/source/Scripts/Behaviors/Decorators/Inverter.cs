﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inverter : Decorator {
	public Inverter(Behavior behavior) : base("Inverter", behavior) {

	}

	public override Status Run(ref Context context) {
		Status status = behavior.Run(ref context);

		if (status == Status.Running) return Status.Running;
		else if (status == Status.Failure) return Status.Failure;
		else if (status == Status.Success) return Status.Success;
		return Status.Invalid;
	}
}
