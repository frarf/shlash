﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RepeatUntilFail : Decorator {
	public RepeatUntilFail(Behavior behavior) : base("RepeatUntilFail", behavior) {

	}

	public override Status Run(ref Context context) {
		Status status = behavior.Update(ref context);

		if (status != Status.Failure) return Status.Running;
		else return Status.Success;
	}
}
