﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timed : Decorator {
	private float originalDuration;
	public float duration;
	protected bool readyToRun;

	public Timed(Behavior behavior, float duration) : base("Timed", behavior) {
		this.duration = duration;
		this.readyToRun = false;
	}

	public override Status Run(ref Context context) {
		if (readyToRun) {
			Status status = behavior.Update(ref context);
			return status;
		}
		else {
			return Status.Running;
		}
	}

	public override void Start(Context context) {
		readyToRun = false;

		Timer.Register(duration, () => readyToRun = true);
	}
}
