﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Root : Behavior {
    protected Behavior behavior;
    protected Context context;

    public Root(GameObject gameObject, Behavior behavior) : base() {
        this.behavior = behavior;
        this.gameObject = gameObject;
        context.gameObject = gameObject;
        context.entity = gameObject.GetComponent<Entity>();
    }

    public Status Update() {   
        return base.Update(ref this.context);
    }

    public override Status Run(ref Context context) {
        return this.behavior.Update(ref this.context);
    }

    public override void Start(Context context) {
    }

    public override void Finish(Status status, Context context) {

    }
}
