﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Selector : Behavior {
	protected string name;
	protected Behavior[] behaviors;
	protected int currentBehavior;

	public Selector(string name, Behavior[] behaviors) : base() {
		this.name = name;
		this.behaviors = behaviors;
		this.currentBehavior = 0;
	}

	public override Status Update(ref Context context) {
		return base.Update(ref context);
	}

	public override Status Run(ref Context context) {
		while (true) {
			Status status = behaviors[currentBehavior].Update(ref context);

			if (status != Status.Failure)
				return status;

			currentBehavior++;

			if (currentBehavior == behaviors.Length)
				return Status.Failure;
		}
	}

	public override void Start(Context context) {
		currentBehavior = 0;
	}
}