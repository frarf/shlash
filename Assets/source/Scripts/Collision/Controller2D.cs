﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof(BoxCollider2D))]

public class Controller2D : Raycaster {
	public BoxCollider2D collider;
	private Corners corners;

	public CollisionInfo collisions;
	public LayerMask collisionMask;
	
	private int horizontalRayCount = 4;
	private int verticalRayCount = 4;	

	private float horizontalRaySpacing;
	private float verticalRaySpacing;

	private const float skinWidth = 0.015f;

	public float maxSlopeAngle = 75;	

	void Awake() {
		collider = GetComponent<BoxCollider2D>();
		CalculateRaySpacing();

		collisions.faceDir = 1;
	}

	public void Move(Vector2 velocity) {
		UpdateRaycastOrigins();
		collisions.Reset();

		if (velocity.x != 0) {
			collisions.faceDir = (int)Mathf.Sign(velocity.x);
		}

		if (velocity.y < 0) {
			DescendSlope(ref velocity);
		}

		HorizontalCollisions (ref velocity);

		if (velocity.y != 0) {
			VerticalCollisions (ref velocity);
		}

		transform.Translate(velocity);
	}

	private void HorizontalCollisions(ref Vector2 velocity) {
		float directionX = collisions.faceDir;
		float rayLength =  Mathf.Abs(velocity.x) + skinWidth;

		if (Mathf.Abs(velocity.x) < skinWidth) {
			rayLength = skinWidth * 2;
		}

		for (int currentRay = 0; currentRay < horizontalRayCount; currentRay++) {
			Vector2 rayOrigin = (directionX == -1) ? corners.bottomLeft : corners.bottomRight;
			rayOrigin += Vector2.up * (horizontalRaySpacing * currentRay);
			RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.right * directionX, rayLength, collisionMask);

			Debug.DrawRay(rayOrigin, Vector2.right * directionX, Color.red);

			if (hit) {
				float slopeAngle = Mathf.Round(Vector2.Angle(hit.normal, Vector2.up));
				bool isBottomRay = currentRay == 0;

				if (isBottomRay && slopeAngle <= maxSlopeAngle) {
					float distanceToSlopeStart = 0;

					if (slopeAngle != collisions.slopeAngleOld) {
						distanceToSlopeStart = hit.distance - skinWidth;
						velocity.x -= distanceToSlopeStart * directionX;
					}

					ClimbSlope(ref velocity, slopeAngle);

					velocity.x += distanceToSlopeStart * directionX;	
				}

				if (!collisions.climbingSlope || slopeAngle > maxSlopeAngle) {
					velocity.x = (hit.distance - skinWidth) * directionX;
					rayLength = hit.distance;

					if (collisions.climbingSlope) {
						velocity.y = Mathf.Tan(collisions.slopeAngle * Mathf.Deg2Rad) * Mathf.Abs(velocity.x);
					}

					collisions.left = directionX == -1;
					collisions.right = directionX == 1;	
				}		

				collisions.slopeAngle = slopeAngle;
				collisions.hit = hit;
			}

		}
	}

	private void VerticalCollisions(ref Vector2 velocity) {
		float directionY = Mathf.Sign(velocity.y);
		float rayLength = Mathf.Abs(velocity.y) + skinWidth;

		for (int currentRay = 0; currentRay < verticalRayCount; currentRay++) {
			Vector2 rayOrigin = (directionY == -1) ? corners.bottomLeft : corners.topLeft;
			rayOrigin += Vector2.right * (verticalRaySpacing * currentRay + velocity.x);
			RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.up * directionY, rayLength, collisionMask);

			Debug.DrawRay(rayOrigin, Vector2.up * -2, Color.red);

			if (hit) {
				velocity.y = (hit.distance - skinWidth) * directionY;
				rayLength = hit.distance;

				if (collisions.climbingSlope) {
					velocity.x = velocity.y / Mathf.Tan(collisions.slopeAngle * Mathf.Deg2Rad) * Mathf.Sign(velocity.x);
				}

				collisions.below = directionY == -1;
				collisions.above = directionY == 1;
				collisions.hit = hit;
			}
		}

		if (collisions.climbingSlope) {
			float directionX = Mathf.Sign(velocity.x);
			rayLength = Mathf.Abs(velocity.x) + skinWidth;
			Vector2 rayOrigin = ((directionX == -1) ? corners.bottomLeft : corners.bottomRight) + Vector2.up * velocity.y;
			RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.right * directionX, rayLength, collisionMask);

			if (hit) {
				float slopeAngle = Mathf.Round(Vector2.Angle(hit.normal, Vector2.up));

				if (slopeAngle != collisions.slopeAngle) {
					velocity.x = (hit.distance - skinWidth) * directionX;
					collisions.slopeAngle = slopeAngle;
				}
			}
		}
	}

	private void ClimbSlope(ref Vector2 velocity, float slopeAngle) {
		float moveDistance = Mathf.Abs(velocity.x);
		float climbVelocityY = Mathf.Sin(slopeAngle * Mathf.Deg2Rad) * moveDistance;

		if (velocity.y <= climbVelocityY) {
			velocity.x = Mathf.Cos(slopeAngle * Mathf.Deg2Rad) * moveDistance * Mathf.Sign(velocity.x);			
			velocity.y = climbVelocityY;

			collisions.below = true;
			collisions.climbingSlope = true;
			collisions.slopeAngle = slopeAngle;
		}
	}

	private void DescendSlope(ref Vector2 velocity) {
		float directionX = Mathf.Sign(velocity.x);
		float rayLength = Mathf.Abs(velocity.y) + Mathf.Abs(velocity.x);
		Vector2 rayOrigin = (directionX == -1) ? corners.bottomRight : corners.bottomLeft;
		RaycastHit2D hit = Physics2D.Raycast (rayOrigin, Vector2.down, rayLength, collisionMask);

		if (hit) {
			float slopeAngle = Vector2.Angle(hit.normal, Vector2.up);
			if (slopeAngle != 0 && slopeAngle <= maxSlopeAngle) {
				if (Mathf.Sign(hit.normal.x) == directionX) {
						float moveDistance = Mathf.Abs(velocity.x);
						float descendVelocityY = Mathf.Sin (slopeAngle * Mathf.Deg2Rad) * moveDistance;
						velocity.x = Mathf.Cos (slopeAngle * Mathf.Deg2Rad) * moveDistance * Mathf.Sign (velocity.x);
						velocity.y -= descendVelocityY;

						collisions.slopeAngle = slopeAngle;
						collisions.descendingSlope = true;
						collisions.below = true;
				}
			}
		}

		Debug.DrawRay(rayOrigin, Vector2.down * -2, Color.green);		
	}

	private void UpdateRaycastOrigins() {
		Bounds bounds = collider.bounds;

		bounds.Expand(skinWidth * -2);

		corners.bottomLeft = new Vector2(bounds.min.x, bounds.min.y);
		corners.bottomRight = new Vector2(bounds.max.x, bounds.min.y);
		corners.topLeft = new Vector2(bounds.min.x, bounds.max.y);
		corners.topRight = new Vector2(bounds.max.x, bounds.max.y);
	}

	private void CalculateRaySpacing() {
		Bounds bounds = collider.bounds;

		bounds.Expand(skinWidth * -2);

		horizontalRayCount = Mathf.Clamp(horizontalRayCount, 2, int.MaxValue);
		verticalRayCount = Mathf.Clamp(horizontalRayCount, 2, int.MaxValue);		

		horizontalRaySpacing = bounds.size.y / (horizontalRayCount - 1);
		verticalRaySpacing = bounds.size.x / (verticalRayCount - 1);		
	}
	
	private struct Corners {
		public Vector2 topLeft, topRight;
		public Vector2 bottomLeft, bottomRight;
	}
}
