﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct CollisionInfo {
		public bool above, below;
		public bool left, right;
		public bool climbingSlope, descendingSlope;
		
		public Vector2 velocityOld;
		public Vector2 hitNormal;
		public RaycastHit2D hit;

		public float slopeAngle, slopeAngleOld;
		public int faceDir;

		public void Reset() {
			above = below = false;
			left = right = false;			
			climbingSlope = descendingSlope = false;

			slopeAngleOld = slopeAngle;
			slopeAngle = 0;

			hitNormal = Vector2.zero;
		}
}
