using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct InputInfo {
    public bool canJumpSquat;
    public bool canJumpRelease;    
    public bool canPrimary, canSecondary;
    public bool canDoubleTapX;
    public Vector2 directionalInput;

    public void DisableInputs() {
        canJumpSquat = false;
        canJumpRelease = false;
        canPrimary = canSecondary = false;
        canDoubleTapX = false;
        directionalInput = Vector2.zero;
    }
}
