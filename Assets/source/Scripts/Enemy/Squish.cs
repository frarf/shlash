﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Controller2D))]
public class Squish : Entity {
	public float directionX = 1;
	public float aroundRadius = 3;
	public LayerMask objectMask;
	private Root behaviorTree;

	public float crawlSpeed = 5;
	public float chaseSpeed = 12;
	public float friction = 1.5f;
	public float gravity = -15;

	private void OnDrawGizmos() {
		Gizmos.color = new Color(1, .5f, .5f, .25f);
		Gizmos.DrawSphere(transform.position, aroundRadius);
	}

	protected override void Start () {
		base.Start();

		steerable = new Steerable(gameObject);

		steerable.arriveSpeed = crawlSpeed;
		steerable.arriveDeaccel = friction;

		behaviorTree = new Root(gameObject, new Selector("idle", new Behavior[] {
			new Sequence("wander", new Behavior[] {
				new FindWanderPoint(-8, 8),
				new MoveToPoint(),			
				new Wait(4)
			})
		}));
	}
	
	protected override void Update () {
		velocity.y += gravity * Time.deltaTime;
		directionX = Mathf.Sign(velocity.x);
		
		base.Update();
		behaviorTree.Update();					

		if (controller.collisions.above || controller.collisions.below)
			velocity.y = 0;
	}
}
