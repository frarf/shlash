﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SquishAnimationController : MonoBehaviour {
	private SpriteRenderer sprite;
	private Animator animator;
	private Squish squish;

	void Start () {
		sprite = GetComponent<SpriteRenderer>();
		animator = GetComponent<Animator>();
		squish = transform.root.gameObject.GetComponent<Squish>();
	}
	
	void Update () {
		sprite.flipX = (squish.controller.collisions.faceDir == 1) ? false : (squish.controller.collisions.faceDir == -1) ? true : false;

		animator.SetFloat("movePercent",  Mathf.Abs(squish.velocity.x) / squish.crawlSpeed);
	}
}
