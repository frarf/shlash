﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof (BoxCollider2D))]
public class Attacker : MonoBehaviour {
	private EntityHitbox hitbox;
	private BoxCollider2D collider;
	[SerializeField] private LayerMask mask;

	public Vector2 knockback;
	public bool resetVelocityX, resetVelocityY;
	public bool allowSideChanging;
	public int damageAmount;
	public float stunTime;

	private void Start () {
		collider = GetComponent<BoxCollider2D>();

		hitbox = new EntityHitbox(new Action<Entity>[]{
			e => e.AddForce(knockback, resetVelocityX, resetVelocityY),
			e => e.Hurt(damageAmount),
			e => e.Stun(stunTime)
		}, collider, mask, false, true);
	}

	private void Update () {
		hitbox.Update();
	}
}
