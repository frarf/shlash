﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Controller2D))]

public class Entity : MonoBehaviour {
    public Controller2D controller;
    public Vector2 velocity;

    protected Dictionary<string, AudioSource> sounds = new Dictionary<string, AudioSource>();

    [SerializeField] protected int maxHealth = 1;
    protected int health = 1;
    protected bool isStunned;
    protected bool isDead;

    public Steerable steerable {get; protected set;}

    public Dictionary<string, bool> behaviorOn = new Dictionary<string, bool>() {
        {"arrive", false}
    };

    protected Vector2 steeringForce;

    protected float arriveSpeed;
    protected float arriveDeaccel;
    private Vector2 arriveTarget;

    protected virtual void Start() {
        controller = GetComponent<Controller2D>();
        health = maxHealth;
    }

    protected virtual void Update() {
        steeringForce *= Time.deltaTime;
        velocity += steeringForce;

        if (steerable != null)
            steerable.Update(ref velocity);

        controller.Move(velocity * Time.deltaTime);
    }

    public virtual void Hurt(int points) {
        health -= points;

        if (health >= maxHealth)
            health = maxHealth;

        if (health <= 0)
            Kill();
    }

    public virtual void Stun(float timeTillUnstun) {
		if (timeTillUnstun != 0) {
			isStunned = true;

			this.AttachTimer(timeTillUnstun, () => isStunned = false);
		}
    }

    protected virtual void Kill() {
        Destroy(gameObject);
    }

    protected void AddSound(string name, AudioSource source) {
        sounds[name] = source;
    }

    protected void PlaySound(string name) {
        sounds[name].Play();
    }

    public virtual void AddForce(Vector2 knockback, bool resetVelocityX = false, bool resetVelocityY = false) {
		if (resetVelocityX) 
			velocity.x = 0;

		if (resetVelocityY)
			velocity.y = 0; 

		velocity += knockback;

        if (knockback.magnitude > 0) {
            if (steerable != null) {
                steerable.ArriveOff();
            }
        }
    }
}
