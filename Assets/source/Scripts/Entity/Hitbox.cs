﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hitbox {
	public RaycastHit2D[] hits;	
	private Collider2D collider;
	private LayerMask mask;
	private Action<RaycastHit2D>[] fns;
	private RaycastHit2D[] oldHits;
	private bool hitSameObjectOnce;
	private bool canCall;

	public Hitbox(Action<RaycastHit2D>[] fns, Collider2D collider, LayerMask mask, bool hitSameObjectOnce = true) {
		this.fns = fns;
		this.collider = collider;
		this.mask = mask;
		this.hitSameObjectOnce = hitSameObjectOnce;
	}
	
	public void Update () {
		oldHits = hits;
		hits = Physics2D.BoxCastAll(collider.transform.position, collider.bounds.size, 0, Vector2.zero, 0, mask);

		foreach (RaycastHit2D hit in hits) {
			canCall = true;			

			if (oldHits != null) {
				foreach(RaycastHit2D oldHit in oldHits) {
					if (hit == oldHit && hitSameObjectOnce)
						canCall = false;
				}
			}

			foreach (Action<RaycastHit2D> fn in fns) 
				if (canCall)
					fn(hit);	
		}
	}
}

public class EntityHitbox {
	public Entity[] entities;
	private Collider2D collider;
	private RaycastHit2D[] hits;
	private LayerMask mask;
	private Action<Entity>[] fns;
	private RaycastHit2D[] oldHits;
	private bool hitSameObjectOnce;
	private bool turnOffAfterHit;
	private bool canCall;

	public EntityHitbox(Action<Entity>[] fns, Collider2D collider, LayerMask mask, bool hitSameObjectOnce = true, bool turnOffAfterHit = true) {
		this.fns = fns;
		this.collider = collider;
		this.mask = mask;
		this.hitSameObjectOnce = hitSameObjectOnce;
		this.turnOffAfterHit = turnOffAfterHit;
	}
	
	public void Update () {
		oldHits = hits;
		hits = Physics2D.BoxCastAll(collider.transform.position, collider.bounds.size, 0, Vector2.zero, 0, mask);
		entities = new Entity[hits.Length];

		for (int i = 0; i < hits.Length; i++) {
			canCall = true;			
			RaycastHit2D hit = hits[i];
			entities[i] = hit.transform.gameObject.GetComponent<Entity>();

			if (oldHits != null && hitSameObjectOnce) {
				foreach(RaycastHit2D old in oldHits) {
					if (hit == old)
						canCall = false;
				}
			}

			foreach (Action<Entity> fn in fns) {
				if (canCall)
					fn(entities[i]);	
			}


			if (turnOffAfterHit)
				collider.gameObject.SetActive(false);
		}
	}
}
