﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitboxManager : MonoBehaviour {
	private Animator animator;
	public Transform attacks;
	private Dictionary<string, Transform> hitboxes = new Dictionary<string, Transform>();

	[SerializeField] private Transform currentAttack;
	[SerializeField] private Transform currentHitbox;
	[SerializeField] private float offsetX;

	private float originX;
	private Transform oldHitbox;

	private string currentAnimationName;
	private string oldAnimationName;

	public Dictionary<string, bool> finished = new Dictionary<string, bool>();

	public Entity hitboxer;
	public int layerLevel;

	private void OnDrawGizmos() {
		if (currentHitbox == null)
			return;

		Gizmos.color = new Color(.5f, 0, .5f, .25f);

		if (currentHitbox.gameObject.GetComponent<Collider2D>() != null)
			Gizmos.DrawCube(currentHitbox.position, currentHitbox.gameObject.GetComponent<Collider2D>().bounds.size);
	}

	private void Start () {
		foreach (Transform attack in attacks) {
			hitboxes[attack.name] = attack;
		}

		animator = GetComponent<Animator>();
	}

	private void Update () {
		oldHitbox = currentHitbox; 	
		
		if (currentHitbox == null)
			return;

		currentHitbox.gameObject.SetActive(true);

		Vector2 desired = new Vector2(offsetX * hitboxer.controller.collisions.faceDir, currentHitbox.localPosition.y);

		currentHitbox.localPosition = desired;
	}

	public void SetAttacks(Transform attacks) {
		this.attacks = attacks;

		hitboxes.Clear();

		foreach (Transform attack in attacks) {
			hitboxes[attack.name] = attack;
		}
	}

	public void SetAttack(string name) {
		currentAttack = hitboxes[name];
	}

	public void SetHitbox(int index) {
		if (oldHitbox != null) {
			oldHitbox.gameObject.SetActive(false);
			oldHitbox.localPosition = new Vector2(offsetX, currentHitbox.localPosition.y);
		}

		finished[GetAnimationName()] = false;	 		

		currentHitbox = currentAttack.GetChild(index);
		offsetX = currentHitbox.localPosition.x;

		oldAnimationName = currentAnimationName;
		currentAnimationName = GetCurrentAnimaton(layerLevel).name;	

		if (index == currentAttack.childCount - 1 && currentAttack.childCount > 1) {
			 this.AttachTimer(Time.deltaTime, Clean);	
			 finished[GetAnimationName()] = true;	 
		}
	}

	public void Clean() {
		if (currentHitbox == null) 
			return;

		finished[oldAnimationName] = true;

		currentHitbox.gameObject.SetActive(false);
		currentAttack = null;
		currentHitbox = null;
	}

	public bool IsFinished(string name) {
		return finished.ContainsKey(name) && finished[name];
	}

	private AnimationClip GetCurrentAnimaton(int layer) {
		AnimatorClipInfo clip = animator.GetCurrentAnimatorClipInfo(layer)[0];

		return clip.clip;
	}

	private string GetAnimationName() {
		AnimatorClipInfo clip = animator.GetCurrentAnimatorClipInfo(layerLevel)[0];

		return clip.clip.name;
	}
}
