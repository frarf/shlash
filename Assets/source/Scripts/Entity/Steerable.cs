﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Steerable {
    private Dictionary<string, bool> behaviorOn = new Dictionary<string, bool>() {
        {"arrive", false}
    };

    private Vector2 steeringForce;

    public float arriveSpeed;
    public float arriveDeaccel;
    private Vector2 arriveTarget;

	private GameObject parent;
	private Transform transform;
	private Vector2 velocity;

	public Steerable(GameObject parent) {
		this.parent = parent;
		this.transform = parent.transform;
	}

	public void Update(ref Vector2 velocity) {
		this.velocity = velocity;

		velocity += CalculateSteering();
	}

    private Vector2 CalculateSteering() {
        Vector2 steeringForce = Vector2.zero;

        if (behaviorOn["arrive"]) steeringForce += Arrive(arriveTarget);

        return steeringForce;
    }

    public void ArriveOn(Vector2 target) {
        behaviorOn["arrive"] = true;
        arriveTarget = target;
    }

    public void ArriveOff() {
        behaviorOn["arrive"] = false;
        arriveTarget = Vector2.zero;
    }

    private Vector2 Arrive(Vector2 target) {
        float deaccel = arriveDeaccel;

        Vector2 toTarget = target - (Vector2)transform.position;
        float distance = toTarget.magnitude;
        float minimumDistance = 0.05f;

        if (distance > minimumDistance) {
            float speed = Mathf.Min(distance / deaccel, arriveSpeed);
            Vector2 desiredVelocity = toTarget * speed / distance;
            return desiredVelocity - velocity;
        }
        else  {
            ArriveOff();
        }

        return Vector2.zero;
    }
}
