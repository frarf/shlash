﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof(Controller2D))]

public class Player : Entity {
	[SerializeField] protected Vector2 velocityOld;
	protected bool descendingSlopeOld;

	public float moveSpeed = 6;
	protected float accelTimeGrounded = 0.13f;
	protected float accelTimeAirborne = 0.1f;
	protected float climbingSlopeFactor = 0;
	protected float descendingSlopeFactor = 0;

	protected float maxJumpHeight = 9;
	protected float minJumpHeight = 4;
	protected float slopeJumpFactorX = 2f;
	protected float slopeJumpFactorY = 1f;

	protected float gravity = -18;		

	[HideInInspector] public float directionX;
	protected float targetVelocityX;
	protected float velocityXSmoothing;	

	protected Vector2 directionalInput;
	public InputInfo buttonInputs {get; protected set;}
	protected PlayerAnimationController anims;	
	public PlayerInput input {get; protected set;}
	protected CameraFollow cameraFollow;

	public bool inputsDisabled;

	protected override void Start () {
		maxHealth = 5;
		base.Start();
		input = GetComponent<PlayerInput>();
		cameraFollow = GameObject.FindWithTag("MainCamera").GetComponent<CameraFollow>();

		cameraFollow.playerInput = input;
		cameraFollow.target = controller;
	}

	protected override void Update () {		
		velocityOld = velocity;
		descendingSlopeOld = controller.collisions.descendingSlope;

		CalculateVelocity();
		
		base.Update();

		inputsDisabled = isStunned;

		if (controller.collisions.above || controller.collisions.below)
			velocity.y = 0;

		directionX = controller.collisions.faceDir;	
	}

	protected virtual void CalculateVelocity() {
		CollisionInfo collisions = controller.collisions;

		int currentSlopeMoveDirection = collisions.descendingSlope ? 1 : collisions.climbingSlope ? -1 : 0;
		float slopeModifier = collisions.slopeAngle * ((collisions.descendingSlope) ? descendingSlopeFactor : (collisions.climbingSlope) ? climbingSlopeFactor : 0);
		float speedModifier = slopeModifier * currentSlopeMoveDirection;
		
		targetVelocityX = (moveSpeed + speedModifier) * directionalInput.x;

		velocity.y += gravity * Time.deltaTime;
		velocity.x = Mathf.SmoothDamp(velocity.x, targetVelocityX, ref velocityXSmoothing, (collisions.below) ? accelTimeGrounded : accelTimeAirborne);			
	}

	public virtual void OnJumpSquat() {
		if (controller.collisions.below) {
			float sy = (controller.collisions.hit.normal.y != 1) ? controller.collisions.hit.normal.y * slopeJumpFactorY : 1;
			velocity.y = maxJumpHeight * sy;
			velocity.x += maxJumpHeight * controller.collisions.hit.normal.x * slopeJumpFactorX;
		}
	}

	public virtual void OnJumpRelease() {
		if (velocity.y > minJumpHeight) {
			velocity.y = minJumpHeight;
		}
	}

	public void SetDirectionalInput(Vector2 input) {
		directionalInput = input;		
	}

	public void SetButtonInputs(InputInfo inputs) {
		buttonInputs = inputs;
	}
}
