﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimationController : MonoBehaviour {
	protected Animator animator;
	protected SpriteRenderer sprite;
	protected Player player;
	protected HitboxManager manager;

	protected virtual void Awake() {
		animator = GetComponent<Animator>();
		sprite = GetComponent<SpriteRenderer>();
		manager = GetComponent<HitboxManager>();
		player = transform.root.gameObject.GetComponent<Player>();
	}

	protected virtual void Update() {
		if (sprite == null) return;

		CollisionInfo collisions = player.controller.collisions;
		Vector2 velocity = player.velocity;
		float moveSpeed = player.moveSpeed;

		float directionX = collisions.faceDir;
		float rotation = 0;

		if (collisions.climbingSlope) {
			if (directionX == 1)
				rotation = collisions.slopeAngle;

			if (directionX == -1)
				rotation = 360 - collisions.slopeAngle;
		}
		else if (collisions.descendingSlope) {
			if (directionX == 1)
				rotation = 360 - collisions.slopeAngle;

			if (directionX == -1)
				rotation = collisions.slopeAngle; 
		}
		else {
			rotation = 0;
		}

		if (player.controller.collisions.below) {
			animator.Play("Movement", 0);
		}
		else if (player.velocity.y > 0) {
			animator.Play("ascend", 0);
		}
		
		bool wallSliding = (collisions.right || collisions.left) && !collisions.below;

		sprite.flipX = (directionX == -1 && !wallSliding) ? true : false;
		transform.eulerAngles = new Vector3(0, 0, rotation);

		animator.SetFloat("speedPercent", !collisions.right || collisions.left ? Mathf.Abs(velocity.x) / moveSpeed : 0);
	}
}
