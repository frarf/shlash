﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour {
	private Player player;	
	public ClackKeyboard keyboard {get; protected set;}
	public InputInfo inputs;

	protected virtual void Start () {
		player = GetComponent<Player>();

		keyboard = new ClackKeyboard();
		
		keyboard.Bind("left", "moveleft");
		keyboard.Bind("right", "moveright");
		keyboard.Bind("up", "lookup");
		keyboard.Bind("down", "lookdown");
		keyboard.Bind("z", "jump");
		keyboard.Bind("x", "action");
	}
	
	protected virtual void Update () {
		keyboard.Update();

		inputs.canPrimary = keyboard.Down("action");
		inputs.canSecondary = keyboard.Down("action") && inputs.directionalInput.y == -1;		
		inputs.canJumpSquat = keyboard.Down("jump");
		inputs.canJumpRelease = keyboard.Released("jump");

		inputs.directionalInput.x = keyboard.Get("moveright") - keyboard.Get("moveleft");
		inputs.directionalInput.y = keyboard.Get("lookup") - keyboard.Get("lookdown");

		if (player.inputsDisabled) {
			inputs.DisableInputs();
		}

		if (inputs.canJumpSquat) {
			player.OnJumpSquat();
		}
		
		if (inputs.canJumpRelease) {
			player.OnJumpRelease();
		}

		player.SetDirectionalInput(inputs.directionalInput);		
		player.SetButtonInputs(inputs);
	}
}
