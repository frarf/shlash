﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSword : Player, IWallSlide {
    [SerializeField] private Vector2[] wallJumpSpeed = new Vector2[]{new Vector2(12, 8), new Vector2(14, 9), new Vector2(16, 10)};    
    [SerializeField] private float[] expThreshold = {150, 300, 600};
	[SerializeField] private float expPoints = 0;
    [SerializeField] private float[] moveSpeeds = {7, 8, 9.5f};
    [SerializeField] private float[] minJumpHeights = {3, 4, 4};
    [SerializeField] private float[] maxJumpHeights = {3, 12, 12};
    [SerializeField] private float gravity = -40;
    [SerializeField] private float descendingSlopeFactor = 0.1f;
    [SerializeField] private float climbingSlopeFactor = 0.025f;
    [SerializeField] private float doubleJumpHeight = 12;
    [SerializeField] private float wallSlideDamp = .25f;    

    private bool canDoubleJump;
    private bool wallSliding;
    private bool wasSliding;
    private int wallDirX;
	public int currentLevel = 1;
	
	protected void Awake () {
		base.Start();	
	}
	
	protected override void Update () {		
		base.Update();

        wasSliding = wallSliding;

		WallSlide();
		HandleExperience();
        HandleValues();

        if (controller.collisions.below) {
            canDoubleJump = true;
        }
	}

    private void HandleValues() {
        moveSpeed = this.moveSpeeds[currentLevel - 1];
        maxJumpHeight = this.maxJumpHeights[currentLevel - 1];
        minJumpHeight = this.minJumpHeights[currentLevel - 1];
        base.gravity = this.gravity;
        base.climbingSlopeFactor = this.climbingSlopeFactor;
        base.descendingSlopeFactor = this.descendingSlopeFactor;
    }

	private void HandleExperience() {
        if (expPoints <= expThreshold[0]) {
            currentLevel = 1;
        }
        else if (expPoints >= expThreshold[1] && !(expPoints >= expThreshold[2])) {
            currentLevel = 2;
        }
        else if (expPoints >= expThreshold[2]){
            currentLevel = 3;
        }
    }

	public void WallSlide() {
		CollisionInfo collisions = controller.collisions;
        wallDirX = (collisions.left) ? -1 : 1;
        wallSliding = false;

        if (!collisions.below && (collisions.right || collisions.left) && collisions.slopeAngle == 90) {
            wallSliding = true;
        }

        if (!wasSliding && wallSliding && velocity.y > 0) {
            velocity.y += Mathf.Abs(velocity.x) * wallSlideDamp;
        }
    }

    public override void OnJumpSquat() {
        base.OnJumpSquat();

         if (wallSliding) {             
            velocity.x = -wallDirX * wallJumpSpeed[currentLevel - 1].x;
            velocity.y = wallJumpSpeed[currentLevel - 1].y;
         }

         if (canDoubleJump && !controller.collisions.below && !wallSliding) {
            velocity.y = doubleJumpHeight;
            canDoubleJump = false;
         }
    }

    public override void OnJumpRelease() {
    	if (velocity.y > minJumpHeight) {
			velocity.y = minJumpHeight;
		}
    }
}
