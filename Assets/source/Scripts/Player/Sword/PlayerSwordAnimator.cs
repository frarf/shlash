﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSwordAnimator : PlayerAnimationController {
	PlayerSword player;

	protected override void Awake () {
		base.Awake();
		player = transform.root.gameObject.GetComponent<PlayerSword>();
		manager.finished["slash_lvl1"] = true;
	}

	protected override void Update () {
		if (sprite == null) return;

		manager.layerLevel = player.currentLevel - 1;

		CollisionInfo collisions = player.controller.collisions;
		Vector2 velocity = player.velocity;
		float moveSpeed = player.moveSpeed;
		float directionX = collisions.faceDir;
		float rotation = 0;
		bool grounded = collisions.below;
		bool wallSliding = (collisions.right || collisions.left) && !collisions.below;

		if (collisions.climbingSlope)  {
			if (directionX == 1)
				rotation = collisions.slopeAngle;

			if (directionX == -1)
				rotation = 360 - collisions.slopeAngle;
		}
		else if (collisions.descendingSlope) {
			if (directionX == 1)
				rotation = 360 - collisions.slopeAngle;

			if (directionX == -1)
				rotation = collisions.slopeAngle; 
		}
		else 
			rotation = 0;
		

		if (grounded && player.input.keyboard.Down("action"))
			Play("slash");

		if (grounded && manager.IsFinished("slash_lvl1")) 
			Play("movement" + player.currentLevel);
		
		if (!grounded && velocity.y > 0 && !wallSliding) 
			Play("ascend");
		
		else if (velocity.y < 0 && !wallSliding) 
			Play("descend");
		
		if (!grounded && collisions.left) 
			Play("wallslideleft");
		
		if (!grounded && collisions.right) 
			Play("wallslideright");
		
		sprite.flipX = (directionX == -1 && !wallSliding) ? true : false;
		transform.eulerAngles = new Vector3(0, 0, rotation);

		animator.SetFloat("speedPercent", !collisions.right || collisions.left ? Mathf.Abs(velocity.x) / moveSpeed : 0);
	}

	private void Play(string name) {
		animator.Play(name, player.currentLevel - 1);
	}

	private string GetAnimationName() {
		AnimatorClipInfo clip = animator.GetCurrentAnimatorClipInfo(player.currentLevel - 1)[0];

		return clip.clip.name;
	}
}
