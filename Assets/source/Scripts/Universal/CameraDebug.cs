﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SubjectNerd.Utilities;

public class CameraDebug : MonoBehaviour {
	public PixelCamera pixelCamera;

	// Use this for initialization
	void Start () {
		pixelCamera = GetComponent<PixelCamera>();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.G)) {
			pixelCamera.enabled = !pixelCamera.enabled;
		}
	}
}
