﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {
	public Controller2D target;
	public PlayerInput playerInput;

	public Vector2 focusAreaSize;
	public float verticalOffset;
	public float lookAmountY;	
	public float lookAheadDistanceX;
	public float lookSmoothTimeX;
	public float verticalSmoothTime;

	private FocusArea focusArea;

	private float currentLookAheadX;
	private float targetLookAheadX;
	private float lookAheadDirX;
	private float smoothLockVelocityX;
	private float smoothLookVelocityY;
	private bool stoppedLookingAhead;
	private float changedVerticalOffset;	
	
	void Start() {
		focusArea = new FocusArea(target.collider.bounds, focusAreaSize);
	}

	void LateUpdate() {
		if (target == null)
			return;

		InputInfo inputs = playerInput.inputs;
		focusArea.Update(target.collider.bounds);

		changedVerticalOffset = verticalOffset + inputs.directionalInput.y * lookAmountY;	

		if (inputs.directionalInput.x != 0) {
			changedVerticalOffset = 0;
		}

		Vector2 focusPosition = focusArea.center + Vector2.up * (changedVerticalOffset);

		if (focusArea.velocity.x != 0) {
			lookAheadDirX = Mathf.Sign(focusArea.velocity.x);

			if (Mathf.Sign(inputs.directionalInput.x) == Mathf.Sign(focusArea.velocity.x) && inputs.directionalInput.x != 0) {
				stoppedLookingAhead = false;
				targetLookAheadX = lookAheadDirX * lookAheadDistanceX;
			}
			else {
				if (!stoppedLookingAhead) {
					stoppedLookingAhead = true;					
					targetLookAheadX = currentLookAheadX + (lookAheadDirX * lookAheadDistanceX - currentLookAheadX) / 4f;
				}
			}
		}

		targetLookAheadX = lookAheadDirX * lookAheadDistanceX;
		currentLookAheadX = Mathf.SmoothDamp(currentLookAheadX, targetLookAheadX, ref smoothLockVelocityX, lookSmoothTimeX);

		focusPosition.y = Mathf.SmoothDamp(transform.position.y, focusPosition.y, ref smoothLookVelocityY, verticalSmoothTime);
		focusPosition += Vector2.right * currentLookAheadX;

		transform.position = ((Vector3)focusPosition + Vector3.forward * -10);
	}

	void OnDrawGizmos() {
		Gizmos.color = new Color(1, 0, 0, .5f);
		Gizmos.DrawCube(focusArea.center, focusAreaSize);
	}

	struct FocusArea {
		public Vector2 center;
		public Vector2 velocity;
		
		float left, right;
		float top, bottom;

		public FocusArea(Bounds targetBounds, Vector2 size) {
			left = targetBounds.center.x - size.x/2;
			right = targetBounds.center.x + size.x/2;
			bottom = targetBounds.min.y;
			top = targetBounds.min.y + size.y;

			velocity = Vector2.zero;
			center = new Vector2((left+right)/2,(top +bottom)/2);
		}

		public void Update(Bounds targetBounds) {
			float shiftX = 0;
			float shiftY = 0;
			
			if (targetBounds.min.x < left) {
				shiftX = targetBounds.min.x - left;
			} 
			else if (targetBounds.max.x > right) {
				shiftX = targetBounds.max.x - right;
			}

			left += shiftX;
			right += shiftX;

			if (targetBounds.min.y < bottom) {
				shiftY = targetBounds.min.y - bottom;
			} 
			else if (targetBounds.max.y > top) {
				shiftY = targetBounds.max.y - top;
			}

			top += shiftY;
			bottom += shiftY;

			center = new Vector2(( left + right ) / 2,( top + bottom) / 2);
			velocity = new Vector2 (shiftX, shiftY); 	
		}
	}
}
